<?php

$publicKey = "-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2mjpLNStkFSTbMKhcIqX
nHqCWrWmgUZvZs8+gejfzOAh2KjMT+i+l2NUEu5egVmIQ6v0QdssDNogVTqKn399
5E3APNnzfDrIRT1L1YQrvpvo/pPP20ZwT8G3UrUY8K+GtEpNg0Io8RMGFy8fOCUa
c39PPk/0SKxPvyTegB0YjxYBC8lMYHzNpmLMgnlHcjxF51iSl02oQySdP463FGCk
pPBbUe1ZjkjXlQ3WZT345DlgsVoDvRe7UEftORMMDQaxdiEmGY7yfEwDbuIaL5sn
e+pEE19YSSuPnRAMDHQVVHE5hoWkuq+tvuOuziAfzRctcyRhBU6HNKVT4yyyMPQJ
lQIDAQAB
-----END PUBLIC KEY-----";

$privateKey = "-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDaaOks1K2QVJNs
wqFwipeceoJataaBRm9mzz6B6N/M4CHYqMxP6L6XY1QS7l6BWYhDq/RB2ywM2iBV
Ooqff33kTcA82fN8OshFPUvVhCu+m+j+k8/bRnBPwbdStRjwr4a0Sk2DQijxEwYX
Lx84JRpzf08+T/RIrE+/JN6AHRiPFgELyUxgfM2mYsyCeUdyPEXnWJKXTahDJJ0/
jrcUYKSk8FtR7VmOSNeVDdZlPfjkOWCxWgO9F7tQR+05EwwNBrF2ISYZjvJ8TANu
4hovmyd76kQTX1hJK4+dEAwMdBVUcTmGhaS6r62+467OIB/NFy1zJGEFToc0pVPj
LLIw9AmVAgMBAAECggEBAL2SmfGjmBLSljOsq9u3L+lOHJ8HVEKZFFC0SZkQvFBj
qn0X5l7lsahNOshDxhuXzsKOGjCfQBjXq4DOk0vlqSvkoJswwjpt+y6+CXiGhEGp
qg5t1diJxyozBh/vuHoQoGcUpzrqzgaRcJPm/r4I1nCxa+K2G16I0Z8jLU0d9Bcc
c6ahh3HpWKdOoLGK47ZvgLtXBg1pkqMaFQViN2/Pg8mfHMKpCT368/u4pTd0JK5I
9iG+F84+Moa4JNo4v61OLvf/3/M1L/NCkHg9yxZVpZGIPqMSZ5EQkKjH1OoWckbt
K2qhZqwUAmTItCGTex8swYYeaR9WvdvaZtCk+K9+OIECgYEA8oAlQKBt8IWMlzT7
PvhGTwnybfIjYHyUMU49+/ZjoGlJ7hB6Hnrezcn3BY02tWzPBW4UKMUR+u74VooB
5odsEgSbQEDGat+s5SLW+KiUQwBBeCLT6jNWgcvcIVLBx0/Zj51DQ5er46qXlFZJ
MIZcz0Gf1+1+5MZa+fQABT0XqF0CgYEA5pFzMse0hT3k/MuKgKYWC/xheXdsxyIw
VC7ZG2Fa16fzzgGJWugxDfUn6b1TcgslkhnwuBSQXCS0prZsIsbC34BKHWv1dyNs
f8x+CbFc/o2QD878LgnFRuBElCW1atmpRtTgu7Imqk2Qc35XAzXFZlXPIykOom/o
zpISTB7DcpkCgYEA6EIl69T8Fz7B3dzHupLuPCtpvzYH/hSzltm6ckWVPAE75sBv
Rhwmo3EJn46V1IQHb8rzkezWAMG6pp/9m/8eWc+BprOjlp7uqHOK85P6lIV9LwQk
dY6AD0biaiQqJqxq0qsrzmRJJH+yOEfAR7KY14SiBJyaSaFVZ0r0bhAKRZ0CgYBD
tccszHT5BBoxGt0LtrqWvoEpmKa7PBERR8i+P7LbzhjNEKpzpehx/uK/w7ezsA5S
f4evp2pVhPoAU2YiOFiWJiidx0yItkBwr278DbgPkScTXWe1ZW0nyUiacRB71nvd
rzpoJIMyVCDtqdaHvMzBpsVzv+TqUw79SuxQqmAfUQKBgDqshCGTEQhT74X/Euv4
6UCagXXLi+YOWJAPA1LiWGI8dPfjTAEIHc7/fpUTdU0Z4rvyCk/0fh9T/I0qHd35
vTTBLFrSBUlEbZ7jssZUSFyu4Cvcg9FuuQTgDSli2rjKFzUwXBd+eyGq2qcLlrGN
7IT85hSqd3KKuhDQX3jEbPZP
-----END PRIVATE KEY-----";

// ideally this will come from an HTTP request body
$data = "GOhYTZQfrHK0tqhl7a9zkbP/jQR5sDSFZViLYENArkl9yQ7/rWs4BFv+LWiUgB+XCb+blWw3xP+C66joXmAUmCWEJd+InlLMnEvo4P+wNlhf171n4grQrRh3yE8gtzbeGJp5/+w+206smNBXbKRBujaW8oN1zPDpZfGTLXg1epeSRMh48fRDaOtLF4TsM9X5UwBSmJJwTUmtQTNNRc5auiS/oD1NBrtfFebQ7fCzWJI6l09qENuiuYJcO2tIT8EIPtR6Zx0GvJqOfX+oAQ7j4XX/l+wyV9kMtq7RzKW8llVJ73ky9QNb3rL/xngNv28ih9KtwKF2D2YoyDmDI/IcLA=="; 

// ideally this will come from an HTTP request header
$signature = "ElcRSvO/YHHrSOiZhGCZtV3U6ewT+X7nKsZRq1x3z8lAdSj3Zbj5S0lpTojSz7A6D8fyjVMs/fx6KgXeAzj0mfdyxqJ9/xayKH54gAkQhH3YDu3CPwG3sAYrB3zJax/9CUzlLaPY522XKaoDIcV1GW28xGVthERqLrt3sK9MkdjolM1e+0+aviqtmtY/S+uPDkvkJGWyYp2n3oweEhS7u+rP7fW8mu8qOc0qiNWlwbaftdg7ylDfVbV/boUA9qEjlYhJxkSlPcdkuyBeEVcdmU0bX9q9DSRBADIYsiUdhVMLvGSpQEgnx6pHKWDClL3s0YpRvgI+m4f18kOmKE2KGg==";

// 1. receiver decrypts the base64 encoded message
$message = base64_decode($data);

// 2. decrypt payload data with private key
openssl_private_decrypt($message, $decryptedData, $privateKey);

echo $decryptedData . PHP_EOL;
// Outputs: {"foo":"bar"}

// 3. receiver decrypts the base64 encoded signature
$rawSignature = base64_decode($signature);

// 4. receiver checks the signature
$varificationCheck = openssl_verify($decryptedData, $rawSignature, $publicKey);

echo $varificationCheck === 1 ? "Signature verified" . PHP_EOL : "Invalid signature" . PHP_EOL;
// Outputs: Signature verified
