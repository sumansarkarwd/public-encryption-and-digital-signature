<?php
// Create a private/public key pair
$config = [
    "digest_alg" => "sha512",
    "private_key_bits" => 2048,
    "private_key_type" => OPENSSL_KEYTYPE_RSA,
];
$resource = openssl_pkey_new($config);

// Extract private key from the pair
openssl_pkey_export($resource, $private_key);

// Extract public key from the pair
$key_details = openssl_pkey_get_details($resource);
$public_key = $key_details["key"];
$bytes = $key_details['bits'] / 8;


$keys = ['private' => $private_key, 'public' => $public_key];

$plaintext = "I have a secret to tell you.";

openssl_public_encrypt($plaintext, $encrypted, $keys['public']);

// Use base64_encode to make contents viewable/sharable
$message = base64_encode($encrypted);

echo PHP_EOL;
echo "Message: $message" . PHP_EOL . PHP_EOL;

echo "Private key base64 encoded:" . base64_encode($private_key) . PHP_EOL . PHP_EOL;
echo "Public key base64 encoded:" . base64_encode($public_key) . PHP_EOL;

/**
 * outputs => 
 * KzDmQJMLQykOxDP/oVpG/cKEerarS8hi/jEmM2pB1dFuFMLViE3+L/sK550iIE6wrGXhebBE4JeHwP
 * Hnar17kUv/1anY7fbM36C/hbU+7ka5tilYquoXGdOvbPqIY0IbIZuDGpWhHTKRPwDEXuKyqWhMEZFq
 * jtwjEmq0TnTPtLO1LVE7cJvPoLOiW6CyzpiI45pEsbG9wUv/bZV+8sjoFL1gakwCgtt9pkUqRtZJI7
 * iDg2fRKQCbHoBYu7m+FBitUnQn0abMrZVTnFmZ1ahS3xDsyVOrbYCqLl7O6lO9ctA9HvcRWe38qnDh
 * jKBVyLM0o6MJ+EF4coolYkey0GV67A==
 */
